package ch.bfh.game;

/***
 * Iterated Prisoners Dilemma Game - main class
 */

public class Project {

	public static void main(String[] args){

		int Strategy1 = 1; //default strategy RANDOM 
		int Strategy2 = 1; //default strategy RANDOM
		int nbGames = 10; // default number of turns

		if (args.length > 0) {
			try {
				Strategy1 = Integer.parseInt(args[0]);
				Strategy2 = Integer.parseInt(args[1]);
				nbGames = Integer.parseInt(args[2]);
			} catch (NumberFormatException e) {
				System.err.println("The 3 arguments must be integers - strategy strategy nbgames");
				System.exit(1);
			}
		}


		// #################### 1b und 1c #######################


		Strategy1 = Strategy.PROB;
		Strategy2 = Strategy.RAND; 
		nbGames = 10;

		//create the two players
		Player Player1 = new Player(Strategy1, nbGames);
		Player Player2 = new Player(Strategy2, nbGames);

		//create game 
		Game currentGame = new Game(Player1, Player2);

		// play game			
		for(int i=0; i<nbGames; i++){
			currentGame.play(i);
		}

		Statistics statistics = new Statistics(Player1, Player2, nbGames);
		System.out.println("\n******* Aufgabe 1b *******\n");
		statistics.calculateRelativeFrequencyOfEvents();
		System.out.println("\n******* Aufgabe 1c *******\n");
		statistics.calculateAverages();

		// #################### 1d #######################

		Strategy1 = Strategy.PROB; 
		Strategy2 = Strategy.RAND; 
		nbGames = 100000;

		//create the two players
		Player1 = new Player(Strategy1, nbGames);
		Player2 = new Player(Strategy2, nbGames);

		//create game 
		currentGame = new Game(Player1, Player2);

		// play game			
		for(int i=0; i<nbGames; i++){
			currentGame.play(i);
		}

		statistics = new Statistics(Player1, Player2, nbGames);
		System.out.println("\n******* Aufgabe 1d *******\n");
		statistics.calculateRelativeFrequencyOfEvents();
		statistics.calculateAverages();

		// #################### 1g #######################


		Strategy1 = Strategy.MAXGAIN; 
		Strategy2 = Strategy.RAND; 
		nbGames = 100000;

		//create the two players
		Player1 = new Player(Strategy1, nbGames);
		Player2 = new Player(Strategy2, nbGames);

		//create game 
		currentGame = new Game(Player1, Player2);

		// play game			
		for(int i=0; i<nbGames; i++){
			currentGame.play(i);
		}

		statistics = new Statistics(Player1, Player2, nbGames);
		System.out.println("\n******* Aufgabe 1g *******\n");
		System.out.println("Strategie von Spieler 1 auf maximalen Gewinn optimiert. \nBetr�gt immer, Wahrscheinlichkeit zu kooperieren: 0");
		statistics.calculateAverages();

		// #################### 2b und 2c #######################

		Strategy1 = Strategy.REAC;  
		Strategy2 = Strategy.RAND; 
		nbGames = 10000;

		//create the two players
		Player1 = new Player(Strategy1, nbGames);
		Player2 = new Player(Strategy2, nbGames);

		//create game 
		currentGame = new Game(Player1, Player2);

		// play game			
		for(int i=0; i<nbGames; i++){
			currentGame.play(i);
		}

		statistics = new Statistics(Player1, Player2, nbGames);
		System.out.println("\n******* Aufgabe 2b *******\n");
		statistics.calculateRelativeFrequencyOfEvents();
		System.out.println("\n******* Aufgabe 2c *******\n");
		statistics.calculateAverages();

		// #################### 2g #######################

		Strategy1 = Strategy.MAXGAIN;  
		Strategy2 = Strategy.REAC; 
		nbGames = 10000;

		//create the two players
		Player1 = new Player(Strategy1, nbGames);
		Player2 = new Player(Strategy2, nbGames);

		//create game 
		currentGame = new Game(Player1, Player2);

		// play game			
		for(int i=0; i<nbGames; i++){
			currentGame.play(i);
		}

		statistics = new Statistics(Player1, Player2, nbGames);
		System.out.println("\n******* Aufgabe 2g *******\n");
		System.out.println("Spieler 1 spielt optimal, wenn Spieler 2 'RAND' spielt.\nSpieler 2 spielt jetzt aber 'REAC'.");
		statistics.calculateRelativeFrequencyOfEvents();
		statistics.calculateAverages();

		// #################### 2h #######################

		Strategy1 = Strategy.MAXGAIN; 
		Strategy2 = Strategy.MAXGAINREACT; 
		nbGames = 10000;

		//create the two players
		Player1 = new Player(Strategy1, nbGames);
		Player2 = new Player(Strategy2, nbGames);

		//create game 
		currentGame = new Game(Player1, Player2);

		// play game			
		for(int i=0; i<nbGames; i++){
			currentGame.play(i);
		}

		statistics = new Statistics(Player1, Player2, nbGames);
		System.out.println("\n******* Aufgabe 2h *******\n");
		System.out.println("Spieler 2 spielt optimal, wenn Spieler 1 'optimal RAND' spielt.\n-> beide betr�gen immer");
		statistics.calculateRelativeFrequencyOfEvents();
		statistics.calculateAverages();

		// #################### 3b und 3c #######################

		Strategy1 = Strategy.ALT; 
		Strategy2 = Strategy.RAND; 
		nbGames = 10000;

		//create the two players
		Player1 = new Player(Strategy1, nbGames);
		Player2 = new Player(Strategy2, nbGames);

		//create game 
		currentGame = new Game(Player1, Player2);

		// play game			
		for(int i=0; i<nbGames; i++){
			currentGame.play(i);
		}

		statistics = new Statistics(Player1, Player2, nbGames);
		System.out.println("\n******* Aufgabe 3b und 3c *******\n");
		statistics.calculateRelativeFrequencyOfEvents();
		statistics.calculateAverages();

		// #################### 4 #######################

		Strategy1 = Strategy.PROB; 
		Strategy2 = Strategy.RAND; 
		nbGames = 100000;

		//create the two players
		Player1 = new Player(Strategy1, nbGames);
		Player2 = new Player(Strategy2, nbGames);

		//create game 
		currentGame = new Game(Player1, Player2);

		// play game			
		for(int i=0; i<nbGames; i++){
			currentGame.play(i);
		}
		statistics = new Statistics(Player1, Player2, nbGames);
		
		System.out.println("\n******* Aufgabe 4 *******\n");
		System.out.println("Erwartungswert vom kummulierten Gewinn: 4.3");
		System.out.println("Erwartungswert von Spieler 1: 2.4");
		System.out.println("Erwartungswert von Spieler 2: 1.9");
		System.out.println("\n10 Wiederholungen: ");
		nbGames = 10;

		//create the two players
		Player1 = new Player(Strategy1, nbGames);
		Player2 = new Player(Strategy2, nbGames);

		//create game 
		currentGame = new Game(Player1, Player2);

		// play game			
		for(int i=0; i<nbGames; i++){
			currentGame.play(i);
		}
		statistics = new Statistics(Player1, Player2, nbGames);
		statistics.calculateAverages();
		System.out.println("\n100 Wiederholungen: ");
		nbGames = 100;

		//create the two players
		Player1 = new Player(Strategy1, nbGames);
		Player2 = new Player(Strategy2, nbGames);

		//create game 
		currentGame = new Game(Player1, Player2);

		// play game			
		for(int i=0; i<nbGames; i++){
			currentGame.play(i);
		}
		statistics = new Statistics(Player1, Player2, nbGames);
		statistics.calculateAverages();
		System.out.println("\n1'000 Wiederholungen: ");
		nbGames = 1000;

		//create the two players
		Player1 = new Player(Strategy1, nbGames);
		Player2 = new Player(Strategy2, nbGames);

		//create game 
		currentGame = new Game(Player1, Player2);

		// play game			
		for(int i=0; i<nbGames; i++){
			currentGame.play(i);
		}
		statistics = new Statistics(Player1, Player2, nbGames);
		statistics.calculateAverages();
		System.out.println("\n10'000 Wiederholungen: ");
		nbGames = 10000;

		//create the two players
		Player1 = new Player(Strategy1, nbGames);
		Player2 = new Player(Strategy2, nbGames);

		//create game 
		currentGame = new Game(Player1, Player2);

		// play game			
		for(int i=0; i<nbGames; i++){
			currentGame.play(i);
		}
		statistics = new Statistics(Player1, Player2, nbGames);
		statistics.calculateAverages();
		System.out.println("\n100'000 Wiederholungen: ");
		nbGames = 100000;

		//create the two players
		Player1 = new Player(Strategy1, nbGames);
		Player2 = new Player(Strategy2, nbGames);

		//create game 
		currentGame = new Game(Player1, Player2);

		// play game			
		for(int i=0; i<nbGames; i++){
			currentGame.play(i);
		}
		statistics = new Statistics(Player1, Player2, nbGames);
		statistics.calculateAverages();
		System.out.println("\n1'000'000 Wiederholungen: ");
		nbGames = 1000000;

		//create the two players
		Player1 = new Player(Strategy1, nbGames);
		Player2 = new Player(Strategy2, nbGames);

		//create game 
		currentGame = new Game(Player1, Player2);

		// play game			
		for(int i=0; i<nbGames; i++){
			currentGame.play(i);
		}
		statistics = new Statistics(Player1, Player2, nbGames);
		statistics.calculateAverages();


		
		
		// #################### 5 #######################

		System.out.println("\n******* Aufgabe 5 *******\n");

		Strategy1 = Strategy.OURSTRATEGY; 
		Strategy2 = Strategy.RAND; 
		int Strategy3 = Strategy.REAC;
		int Strategy4 = Strategy.ALT;
		
		nbGames = 100000;

		//create the two players
		
		Player Player1_1 = new Player(Strategy1, nbGames);
		Player Player1_2 = new Player(Strategy1, nbGames);
		Player Player1_3 = new Player(Strategy1, nbGames);
		Player Player1_4 = new Player(Strategy1, nbGames);
		Player Player1_5 = new Player(Strategy1, nbGames);
		
		Player2 = new Player(Strategy2, nbGames);
		Player Player3 = new Player(Strategy3, nbGames);
		Player Player4 = new Player(Strategy4, nbGames);

		//create game 
		Game game1 = new Game(Player1_1, Player2);
		Game game2 = new Game(Player1_2, Player3);
		Game game3 = new Game(Player1_3, Player4);
		Game game4 = new Game(Player1_4, Player1_5);
		
		// play game			
		for(int i=0; i<nbGames; i++){
			game1.play(i);
			game2.play(i);
			game3.play(i);
			game4.play(i);
		}
		

		Statistics statistics1 = new Statistics(Player1_1, Player2, nbGames);
		Statistics statistics2 = new Statistics(Player1_2, Player3, nbGames);
		Statistics statistics3 = new Statistics(Player1_3, Player4, nbGames);
		Statistics statistics4 = new Statistics(Player1_4, Player1_5, nbGames);
		
		System.out.println("Unsere Strategie gegen RAND:\n");
		statistics1.calculateRelativeFrequencyOfEvents();
		statistics1.calculateAverages();
		
		System.out.println("\nUnsere Strategie gegen REAC:\n");
		statistics2.calculateRelativeFrequencyOfEvents();
		statistics2.calculateAverages();
		
		System.out.println("\nUnsere Strategie gegen ALT:\n");
		statistics3.calculateRelativeFrequencyOfEvents();
		statistics3.calculateAverages();
		
		System.out.println("\nUnsere Strategie gegen sich selbst:\n");
		statistics4.calculateRelativeFrequencyOfEvents();
		statistics4.calculateAverages();
	}

}

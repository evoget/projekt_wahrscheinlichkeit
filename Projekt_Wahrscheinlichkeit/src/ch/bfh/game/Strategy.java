package ch.bfh.game;

/*
 * Strategy: implement different strategies
 * 
 * 1 - RAND = collaborate with probability 0.5
 * 2 - PROB = collaborate with probability p 
 * 3 - ALT =  collaborate with probability p_CC if he has collaborated the last turn and with a probability p_CD if he has defected. 
 * 4 - REAC = collaborate with probability p_CC if the other player has collaborated the last turn and with a probability p_CD if he has defected. 
 * 5 - WIN =  redo same action with probability p_R if he has won more than the other player the last turn  
 * 6 - OWN =  *describe here your own strategy*
 */


public class Strategy {

	//type of strategy
	final public static int RAND = 1;	
	final public static int PROB = 2;	
	final public static int ALT = 3;	
	final public static int REAC = 4;	
	final public static int WIN = 5;	
	final public static int OWN = 6;	
	final public static int MAXGAIN = 7;
	final public static int MAXGAINREACT = 8;
	final public static int OURSTRATEGY = 9;

	// possible MOVEs
	final static int COOPERATE = 1;
	final static int DEFECT = 0;

	//***************PART TO BE MODIFIED = variable needed for your strategy *************************	



	//*************************************************************************************************	

	// compute next move according to strategy
	public static int nextMove(Player Player1, Player Player2, int nGame){

		int Move=-1;
		switch(Player1.PlayerStrategy){

		case RAND:
			Move = (int) (Math.random() + 0.5); //random number between 0.5 and 1.5 casted to int
			break;

			//************PART TO BE MODIFIED = implement other strategies here ***********************************	

		case PROB:
			/* ######################
			 * Aufgabe 1a
			 * ######################
			 */
			if (Math.random() < 0.4) Move = COOPERATE;
			else Move = DEFECT;
			break;

		case REAC:
			/* ######################
			 * Aufgabe 2a
			 * ######################
			 */
			Move = DEFECT;
			int[] player2MoveHistory = Player2.getMoveHistory();
			if (nGame == 0){
				if (Math.random() < 0.5) Move = COOPERATE;
				break;
			}
			if (player2MoveHistory[nGame-1] == COOPERATE) {
				if (Math.random() < 0.6) Move = COOPERATE;
			} else {
				if (Math.random() < 0.35) Move = COOPERATE;
			}
			break;

		case ALT:
			/* ######################
			 * Aufgabe 3a
			 * ######################
			 */
			Move = DEFECT;
			int[] player1MoveHistory = Player1.getMoveHistory();
			if (nGame == 0){
				if (Math.random() < 0.5) Move = COOPERATE;
				break;
			}
			if (player1MoveHistory[nGame-1] == COOPERATE) {
				if (Math.random() < 0.4) Move = COOPERATE;
			} else {
				if (Math.random() < 0.65) Move = COOPERATE;
			}
			break;

			
		case MAXGAIN:
			/* ######################
			 * Aufgabe 1g
			 * ######################
			 */
			Move = DEFECT;
			
			break;
		case MAXGAINREACT:
			/* ######################
			 * Aufgabe 2h
			 * ######################
			 */
			Move = DEFECT;
			break;
			
		case OURSTRATEGY:
			/* ######################
			 * Aufgabe 5
			 * ######################
			 */
			if (nGame == 0) Move = COOPERATE;
			else {
				int[] otherplayerMoveHistory = Player2.getMoveHistory();
				if (otherplayerMoveHistory[nGame-1] == COOPERATE) Move = COOPERATE;
				else Move = DEFECT;
			}
			break;
			
		//**********************************************************************************
			
		default:
			System.out.println("Strategy not implemented\n");
			System.exit(-1);
			break;
		}

		return Move;    	   
	}

}

package ch.bfh.game;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

/****
 *  Statistics - implement methods to get relative frequencies, middle gain, ... 
 */

public class Statistics {
	
	public static int CooperateCooperate= 0; 
	public static int DefectCooperate= 0; 
	public static int CooperateDefect= 0; 
	public static int DefectDefect= 0; 	
	
	private Player player1;
	private Player player2;
	private int nbGames;
	
//***************PART TO BE MODIFIED = VARIABLES AND FUNCTIONS FOR STAT *************************	
	
public Statistics(Player player1, Player player2, int nbGames){
	this.player1 = player1;
	this.player2 = player2;
	this.nbGames = nbGames;
}

public void calculateRelativeFrequencyOfEvents(){
	int[] MoveHistoryPlayer1 = player1.getMoveHistory();
	int[] MoveHistoryPlayer2 = player2.getMoveHistory();
	
	int event11 = 0;
	int event10 = 0;
	int event01 = 0;
	int event00 = 0;
	
	for(int i=0; i<nbGames; i++){
		if (MoveHistoryPlayer1[i] == 1 && MoveHistoryPlayer2[i] == 1) event11++;
		if (MoveHistoryPlayer1[i] == 1 && MoveHistoryPlayer2[i] == 0) event10++;
		if (MoveHistoryPlayer1[i] == 0 && MoveHistoryPlayer2[i] == 1) event01++;
		if (MoveHistoryPlayer1[i] == 0 && MoveHistoryPlayer2[i] == 0) event00++;
	}
	
	
	System.out.println("Ereignis (1,1): "+((double)event11)/nbGames);
	System.out.println("Ereignis (1,0): "+((double)event10)/nbGames);
	System.out.println("Ereignis (0,1): "+((double)event01)/nbGames);
	System.out.println("Ereignis (0,0): "+((double)event00)/nbGames);
}


public void calculateAverages(){
	float[] GainHistoryPlayer1 = player1.getGainHistory();
	float[] GainHistoryPlayer2 = player2.getGainHistory();
	
	float gainTogether = 0;
	float gainPlayer1 = 0;
	float gainPlayer2 = 0;
	
	for(int i=0; i<nbGames; i++){
		gainTogether += (GainHistoryPlayer1[i] + GainHistoryPlayer2[i]);
		gainPlayer1 += GainHistoryPlayer1[i];
		gainPlayer2 += GainHistoryPlayer2[i];
	}
	
	float avgGainTogether = gainTogether / nbGames;
	float avgGainPlayer1 = gainPlayer1 / nbGames;
	float avgGainPlayer2 = gainPlayer2 / nbGames;
//	double avgGainTogether = gainTogether ;
//	double avgGainPlayer1 = gainPlayer1 ;
//	double avgGainPlayer2 = gainPlayer2 ;
	
	System.out.println("Kummulierter durchschnittlicher Gewinn: " + avgGainTogether);
	System.out.println("Durchschnittlicher Gewinn Spieler 1: " + avgGainPlayer1);
	System.out.println("Durchschnittlicher Gewinn Spieler 2: " + avgGainPlayer2);
}




//*************************************************************************************************	


}
